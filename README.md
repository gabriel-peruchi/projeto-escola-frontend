# Frontend do projeto escola desenvolvido em React e Material Ui

Projeto desenvolvido para a matéria de JPW do cuso de Ciência da Computação da UNESC
## Scripts Disponíveis

No diretório do projeto, você pode executar:

### `yarn start`

Executa o aplicativo no modo de desenvolvimento. \
Abra [http: // localhost: 3000] (http: // localhost: 3000) para visualizá-lo no navegador.

A página será recarregada se você fizer edições. \
Você também verá quaisquer erros de lint no console.

## Modelos do projeto

Modelos e funcionalidades presentes no projeto.

### Disciplina

Operações básicas de CRUD disponível.

### Estudante

Operações básicas de CRUD disponível.

### Professor

Operações básicas de CRUD disponível.

### Turma

Operações básicas de CRUD disponível.

### Matrícula

Operações básicas de CRUD disponível.

### Dashboard

Dashboard das turmas em conjunto com professor, disciplina, matrículas e estudantes.

## API

Utilização em conjunto com a API em `https://gitlab.com/gabriel-peruchi/projeto-escola`.

