import { GenericService } from '../core/services/GenericService'
export default class ProfessorService extends GenericService {
    constructor() {
        super('professores')
    }
}
