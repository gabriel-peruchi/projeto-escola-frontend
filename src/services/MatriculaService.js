import { GenericService } from '../core/services/GenericService'
export default class MatriculaService extends GenericService {
    constructor() {
        super('matriculas')
    }
}
