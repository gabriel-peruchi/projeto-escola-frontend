import { GenericService } from '../core/services/GenericService'
export default class TurmaService extends GenericService {
    constructor() {
        super('turmas')
    }
}
