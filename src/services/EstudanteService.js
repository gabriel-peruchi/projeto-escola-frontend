import { GenericService } from "../core/services/GenericService";

export default class EstudanteService extends GenericService {
    constructor() {
        super('estudantes')
    }
}
