import { GenericService } from '../core/services/GenericService';

export default class DashboardService extends GenericService {
    constructor() {
        super('dashboard')
    }
}
