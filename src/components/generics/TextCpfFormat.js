import React from "react";
import MaskedInput from "react-text-mask";

export default class TextCpfFormat extends React.Component {
    render() {
        const { inputRef, ...other } = this.props;

        return (
            <MaskedInput
                {...other}
                ref={(ref) => {
                    inputRef(ref ? ref.inputElement : null);
                }}
                mask={[/\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/]}
                placeholderChar={'\u2000'}
                showMask
            />
        );
    }
}