import { Slide, Snackbar } from '@material-ui/core'
import { Alert } from '@material-ui/lab'
import React from 'react'

export default class SnackbarFeedback extends React.Component {
    render() {
        return (
            <Snackbar
                open={this.props.open}
                TransitionComponent={Slide}
                autoHideDuration={3000}
                onClose={this.props.onClose}>
                <Alert
                    variant="filled"
                    severity={this.props.type}
                    onClose={this.props.onClose}>
                    {this.props.message}
                </Alert>
            </Snackbar>
        )
    }
}