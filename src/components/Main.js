import {
    AppBar,
    Container,
    CssBaseline,
    Divider,
    Drawer,
    Hidden,
    Icon,
    IconButton,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Toolbar,
    Typography,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { Menu } from '@material-ui/icons';
import React from 'react';
import { BrowserRouter, NavLink, Redirect, Route } from 'react-router-dom';

import Dashboard from './dashboard/Dashboard';
import Disciplina from './disciplina/Disciplina';
import Estudante from './estudante/Estudante';
import SnackbarFeedback from './generics/SanckbarFeedback';
import Matricula from './matricula/Matricula';
import Professor from './professor/Professor';
import Turma from './turma/Turma';

const drawerWidth = 240;

const useStyles = (theme) => ({
    root: {
        display: 'flex',
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    appBar: {
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
        },
    },
    menuButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginLeft: 10,
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
});

class LayoutMain extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            snackBar: {
                open: false,
                message: "",
                type: "info"
            },
            mobileOpen: false,
            pathCurrent: window.location.pathname
        }
    }

    showSnackBar = (message, type) => {
        this.setState({
            snackBar: { open: true, message, type }
        })
    }

    closeSnackBar = (event, reason) => {
        if (reason === 'clickaway') {
            return
        }

        this.setState({
            snackBar: { ...this.state.snackBar, open: false }
        })
    }

    handlerSelectedOptionMenu = (to) => {
        this.setState({ pathCurrent: to, mobileOpen: false })
    }

    handlerDrawer = () => {
        this.setState({ mobileOpen: !this.state.mobileOpen })
    }

    optionsMenu = [
        { label: 'Dashboard', to: '/dashboard', childrens: ['/'], icon: 'dashboard' },
        { label: 'Estudantes', to: '/estudantes', childrens: [], icon: 'face' },
        { label: 'Professores', to: '/professores', childrens: [], icon: 'assignment_ind' },
        { label: 'Disciplinas', to: '/disciplinas', childrens: [], icon: 'menu_book' },
        { label: 'Turmas', to: '/turmas', childrens: [], icon: 'supervised_user_circle' },
        { label: 'Matriculas', to: '/matriculas', childrens: [], icon: 'fact_check' },
    ]

    render() {
        const { classes, theme, window } = this.props;

        const container = window !== undefined ? () => window().document.body : undefined;

        const drawer = (
            <React.Fragment>
                <div className={classes.toolbar}>
                    <Icon style={{ marginRight: 10 }}>{'menu'}</Icon>
                    <Typography variant="h6" noWrap>
                        Menu
                    </Typography>
                </div>
                <Divider />
                <List>
                    {this.optionsMenu.map((optionMenu) => (
                        <ListItem
                            button
                            selected={this.state.pathCurrent === optionMenu.to || optionMenu.childrens.includes(this.state.pathCurrent)}
                            component={NavLink}
                            to={optionMenu.to}
                            key={optionMenu.label}
                            onClick={() => this.handlerSelectedOptionMenu(optionMenu.to)}>
                            <ListItemIcon>
                                <Icon>{optionMenu.icon}</Icon>
                            </ListItemIcon>
                            <ListItemText primary={optionMenu.label} />
                        </ListItem>
                    ))}
                </List>
            </React.Fragment>
        )

        return (
            <div className={classes.root}>
                <CssBaseline />
                <BrowserRouter>
                    <AppBar
                        position="fixed"
                        className={classes.appBar}
                    >
                        <Toolbar>
                            <IconButton
                                color="inherit"
                                aria-label="open drawer"
                                onClick={this.handlerDrawer}
                                edge="start"
                                className={classes.menuButton}
                            >
                                <Menu />
                            </IconButton>
                            <Icon style={{ marginRight: 10 }} fontSize='large'>{'school'}</Icon>
                            <Typography variant="h5" noWrap>
                                Projeto Escola
                            </Typography>
                        </Toolbar>
                    </AppBar>
                    <nav className={classes.drawer} aria-label="mailbox folders">
                        <Hidden smUp implementation="css">
                            <Drawer
                                container={container}
                                variant="temporary"
                                anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                                open={this.state.mobileOpen}
                                onClose={this.handlerDrawer}
                                classes={{ paper: classes.drawerPaper }}
                                ModalProps={{ keepMounted: true }}
                            >
                                {drawer}
                            </Drawer>
                        </Hidden>
                        <Hidden xsDown implementation="css">
                            <Drawer
                                classes={{ paper: classes.drawerPaper }}
                                variant="permanent"
                                open
                            >
                                {drawer}
                            </Drawer>
                        </Hidden>
                    </nav>
                    <Container
                        maxWidth="lg"
                        style={{ marginTop: 50, marginBottom: 30 }}>
                        <CssBaseline />
                        <main>
                            <Toolbar />
                            <Route exact path="/">
                                <Redirect to="/dashboard" />
                            </Route>
                            <Route path="/dashboard">
                                <Dashboard showSnackBar={this.showSnackBar}></Dashboard>
                            </Route>
                            <Route path="/estudantes">
                                <Estudante showSnackBar={this.showSnackBar}></Estudante>
                            </Route>
                            <Route path="/professores">
                                <Professor showSnackBar={this.showSnackBar}></Professor>
                            </Route>
                            <Route path="/disciplinas">
                                <Disciplina showSnackBar={this.showSnackBar}></Disciplina>
                            </Route>
                            <Route path="/turmas">
                                <Turma showSnackBar={this.showSnackBar}></Turma>
                            </Route>
                            <Route path="/matriculas">
                                <Matricula showSnackBar={this.showSnackBar}></Matricula>
                            </Route>
                            <SnackbarFeedback
                                open={this.state.snackBar.open}
                                type={this.state.snackBar.type}
                                message={this.state.snackBar.message}
                                onClose={this.closeSnackBar} />
                        </main>
                    </Container>
                </BrowserRouter>
            </div >
        )
    }
}

export default withStyles(useStyles, { withTheme: true })(LayoutMain);