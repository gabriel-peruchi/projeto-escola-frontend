import {
    Checkbox,
    IconButton,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
} from '@material-ui/core';
import { Delete } from '@material-ui/icons';
import React from 'react';
import '../../App.css'

export default class ListagemDisciplinas extends React.Component {
    handlerSelect = async (disciplina) => {
        if (this.isItemSelected(disciplina._id)) {
            this.refreshSelected()
            return
        }

        await this.props.selectDisciplina(disciplina)
    }

    isItemSelected = (id) => {
        if (!this.props.selected) {
            return false
        }

        return this.props.selected._id === id
    }

    refreshSelected = async () => await this.props.selectDisciplina(null)

    deleteDisciplina = (id) => {
        if (this.isItemSelected(id)) {
            this.refreshSelected()
        }

        this.props.deleteDisciplina(id)
    }

    columns = [
        { label: 'Nome' }
    ]

    render() {
        return (
            <Paper>
                <TableContainer>
                    <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>
                                <TableCell className="TableCellHead"></TableCell>
                                {this.columns.map((column) => {
                                    return <TableCell className="TableCellHead" key={column.label}>{column.label}</TableCell>
                                })}
                                <TableCell className="TableCellHead"></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.props.disciplinas.map((d) => {
                                return <TableRow hover key={d._id}>
                                    <TableCell padding="checkbox">
                                        <Checkbox
                                            checked={this.isItemSelected(d._id)}
                                            onChange={() => this.handlerSelect(d)} />
                                    </TableCell>
                                    <TableCell component="th" scope="row">
                                        {d.nome}
                                    </TableCell>
                                    <TableCell align="right">
                                        <IconButton
                                            aria-label="delete"
                                            onClick={() => this.deleteDisciplina(d._id)}>
                                            <Delete fontSize="default" />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
        )
    }
}