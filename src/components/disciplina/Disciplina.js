import '../../App.css';

import { TextField, Typography } from '@material-ui/core';
import { debounce } from 'lodash';
import React from 'react';

import MessageErrorStatus from '../../core/utils/MessageErrorStatus';
import DisciplinaService from '../../services/DisciplinaService';
import FormDisciplina from './FormDisciplina';
import ListagemDisciplinas from './ListagemDisciplinas';

const disciplinaService = new DisciplinaService()
export default class Disciplina extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            disciplinas: [],
            selected: null,
            search: ""
        }
    }

    componentDidMount = async () => {
        await this.refreshDisciplinas()
    }

    selectDisciplina = async (disciplina) => {
        if (!disciplina) {
            this.setState({ selected: null })
            return
        }

        try {
            const disciplinaDB = await disciplinaService.findById(disciplina._id)
            this.setState({ selected: disciplinaDB })
        } catch (error) {
            this.props.showSnackBar(MessageErrorStatus(error), 'error')
        }
    }

    handlerSearch = (event) => {
        this.setState({ search: event.target.value })
        debounce(this.refreshDisciplinas, 300)()
    }

    refreshDisciplinas = async () => {
        try {
            const disciplinas = await disciplinaService.findAllDocs({ term: this.state.search })
            this.setState({ disciplinas, selected: null })
        } catch (error) {
            this.props.showSnackBar(MessageErrorStatus(error), 'error')
        }
    }

    saveDisciplina = async (disciplina) => {
        try {
            if (this.state.selected) {
                await disciplinaService.update(this.state.selected._id, disciplina)
            } else {
                await disciplinaService.post(disciplina)
            }

            this.props.showSnackBar("Salvo com sucesso!", "success")
        } catch (error) {
            this.props.showSnackBar(MessageErrorStatus(error), 'error')
        }

        await this.refreshDisciplinas()
    }

    deleteDisciplina = async (id) => {
        try {
            await disciplinaService.delete(id)
            this.props.showSnackBar("Excluido com sucesso!", "success")
        } catch (error) {
            this.props.showSnackBar(MessageErrorStatus(error), 'error')
        }

        await this.refreshDisciplinas()
    }

    render() {
        return (
            <React.Fragment>
                <Typography className="TitlePage" variant="h4" noWrap>
                    Disciplinas
                </Typography>
                <FormDisciplina
                    selected={this.state.selected}
                    key={this.state.selected ? this.state.selected._id : null}
                    saveDisciplina={this.saveDisciplina}>
                </FormDisciplina>
                <TextField
                    style={{ marginBottom: 10 }}
                    name="search"
                    value={this.state.search}
                    onChange={this.handlerSearch}
                    label="Buscar"
                    variant="outlined"
                    size="small" />
                <ListagemDisciplinas
                    disciplinas={this.state.disciplinas}
                    deleteDisciplina={this.deleteDisciplina}
                    selectDisciplina={this.selectDisciplina}
                    selected={this.state.selected}>
                </ListagemDisciplinas>
            </React.Fragment>
        )
    }
}