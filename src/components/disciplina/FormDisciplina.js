import '../../App.css';

import { Button, Grid, TextField } from '@material-ui/core';
import React from 'react';

export default class FormDisciplina extends React.Component {
    constructor(props) {
        super(props)

        if (!this.props.selected) {
            this.state = {
                disciplina: { nome: "" },
                validations: {
                    inputNameError: false
                }
            }
        } else {
            this.state = {
                disciplina: this.props.selected,
                validations: {
                    inputNameError: false
                }
            }
        }
    }

    handlerInput = (event) => {
        this.setState({
            disciplina: {
                ...this.state.disciplina,
                [event.target.name]: event.target.value
            }
        })
    }

    saveDisciplina = () => {
        if (!this.validationInputs()) return

        this.props.saveDisciplina(this.state.disciplina)
        this.clearForm()
    }

    clearForm = () => {
        this.setState({
            disciplina: { nome: "" },
            validations: { inputNameError: false }
        })
    }

    validationInputs = () => {
        if (!this.state.disciplina.nome.trim()) {
            this.setState({
                validations: {
                    ...this.state.validations,
                    inputNameError: true
                }
            })
            return false
        }

        return true
    }

    render() {
        return (
            <div style={{ flexGrow: 1 }}>
                <Grid
                    container
                    justify="center"
                    spacing={2}
                    style={{ marginBottom: 50 }}>
                    <Grid item lg={4}>
                        <TextField
                            name="nome"
                            className="TextField"
                            required
                            value={this.state.disciplina.nome}
                            onChange={this.handlerInput}
                            error={this.state.validations.inputNameError}
                            helperText={this.state.validations.inputNameError ? "Campo obrigatório" : null}
                            label="Disciplina"
                            variant="outlined"
                            size="small" />
                    </Grid>
                    <Grid
                        container
                        justify="center"
                        style={{ marginTop: 20 }}>
                        <Grid item lg={2}>
                            <Button
                                className="ButtonSave"
                                variant="contained"
                                color="primary"
                                onClick={this.saveDisciplina}>
                                {this.props.selected ? <>Salvar</> : <>Inserir</>}
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        )
    }
}