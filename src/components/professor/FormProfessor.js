import '../../App.css';

import DateFnsUtils from '@date-io/date-fns';
import { Button, Grid, MenuItem, TextField } from '@material-ui/core';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import React from 'react';

import TextCellPhoneFormat from '../generics/TextCellPhoneFormat';
import TextCpfFormat from '../generics/TextCpfFormat';
import TextPhoneFormat from '../generics/TextPhoneFormat';

export default class FormProfessor extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            professor: this.props.selected ? this.props.selected : {
                nome: "",
                sobrenome: "",
                cpf: "",
                dataNascimento: new Date(),
                email: "",
                celular: "",
                telefoneFixo: "",
                grauAcademico: "",
                experiencia: ""
            },

            validations: {
                inputNomeError: false,
                inputSobrenomeError: false,
                inputCpfError: false,
                inputEmailError: false,
                inputCelularError: false,
                inputGrauAcademicoError: false,
                inputExperienciaError: false
            }
        }
    }

    handlerInputText = (event) => {
        this.setState({
            professor: {
                ...this.state.professor,
                [event.target.name]: event.target.value
            }
        })
    }

    handlerInputDate = (date, value) => {
        this.setState({
            professor: {
                ...this.state.professor,
                dataNascimento: date
            }
        })
    }

    saveProfessor = () => {
        if (!this.validationInputs()) return

        this.props.saveProfessor(this.state.professor)
        this.clearForm()
    }

    clearForm = () => {
        this.setState({
            professor: {
                nome: "",
                sobrenome: "",
                cpf: "",
                dataNascimento: new Date(),
                email: "",
                celular: "",
                telefoneFixo: "",
                grauAcademico: "",
                experiencia: ""
            },
            validations: {
                inputNomeError: false,
                inputSobrenomeError: false,
                inputCpfError: false,
                inputEmailError: false,
                inputCelularError: false,
                inputGrauAcademicoError: false,
                inputExperienciaError: false
            }
        })
    }

    validationInputs = () => {
        const validationErrors = {}
        let ok = true

        Object.keys(this.state.professor)
            .forEach((key) => {
                const keyUpperFirst = key[0].toUpperCase() + key.substr(1);
                const keyValidationInputError = `input${keyUpperFirst}Error`

                if (this.state.validations[keyValidationInputError] === undefined) {
                    return
                }

                validationErrors[keyValidationInputError] = !this.state.professor[key].trim()

                if (!this.state.professor[key].trim()) {
                    ok = false
                }
            })

        this.setState({ validations: validationErrors })

        return ok
    }

    render() {
        return (
            <div style={{ flexGrow: 1 }}>
                <Grid
                    container
                    spacing={2}
                    style={{ marginBottom: 50 }}>
                    <Grid item lg={4}>
                        <TextField
                            name="nome"
                            className="TextField"
                            required
                            value={this.state.professor.nome}
                            onChange={this.handlerInputText}
                            error={this.state.validations.inputNomeError}
                            helperText={this.state.validations.inputNomeError ? "Campo obrigatório" : null}
                            label="Nome"
                            variant="outlined"
                            size="small" />
                    </Grid>
                    <Grid item lg={4}>
                        <TextField
                            name="sobrenome"
                            className="TextField"
                            required
                            value={this.state.professor.sobrenome}
                            onChange={this.handlerInputText}
                            error={this.state.validations.inputSobrenomeError}
                            helperText={this.state.validations.inputSobrenomeError ? "Campo obrigatório" : null}
                            label="Sobrenome"
                            variant="outlined"
                            size="small" />
                    </Grid>
                    <Grid item lg={4}>
                        <TextField
                            name="cpf"
                            className="TextField"
                            required
                            value={this.state.professor.cpf}
                            onChange={this.handlerInputText}
                            error={this.state.validations.inputCpfError}
                            helperText={this.state.validations.inputCpfError ? "Campo obrigatório" : null}
                            InputProps={{ inputComponent: TextCpfFormat }}
                            label="CPF"
                            variant="outlined"
                            size="small" />
                    </Grid>
                    <Grid item lg={4}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                className="TextField"
                                required
                                variant="inline"
                                inputVariant="outlined"
                                size="small"
                                format="dd/MM/yyyy"
                                label="Data de Nacimento"
                                name="dataNascimento"
                                value={this.state.professor.dataNascimento}
                                onChange={this.handlerInputDate}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item lg={4}>
                        <TextField
                            name="celular"
                            className="TextField"
                            required
                            value={this.state.professor.celular}
                            onChange={this.handlerInputText}
                            error={this.state.validations.inputCelularError}
                            helperText={this.state.validations.inputCelularError ? "Campo obrigatório" : null}
                            InputProps={{ inputComponent: TextCellPhoneFormat }}
                            label="Celular"
                            variant="outlined"
                            size="small" />
                    </Grid>
                    <Grid item lg={4}>
                        <TextField
                            name="telefoneFixo"
                            className="TextField"
                            value={this.state.professor.telefoneFixo}
                            onChange={this.handlerInputText}
                            error={this.state.validations.inputTelefoneFixoError}
                            helperText={this.state.validations.inputTelefoneFixoError ? "Campo obrigatório" : null}
                            InputProps={{ inputComponent: TextPhoneFormat }}
                            label="Telefone Fixo"
                            variant="outlined"
                            size="small" />
                    </Grid>
                    <Grid item lg={4}>
                        <TextField
                            name="email"
                            className="TextField"
                            required
                            value={this.state.professor.email}
                            onChange={this.handlerInputText}
                            error={this.state.validations.inputEmailError}
                            helperText={this.state.validations.inputEmailError ? "Campo obrigatório" : null}
                            label="Email"
                            variant="outlined"
                            size="small" />
                    </Grid>
                    <Grid item lg={4}>
                        <TextField
                            required
                            name="grauAcademico"
                            className="TextField"
                            size="small"
                            select
                            label="Grau acadêmico"
                            value={this.state.professor.grauAcademico}
                            error={this.state.validations.inputGrauAcademicoError}
                            helperText={this.state.validations.inputGrauAcademicoError ? "Campo obrigatório" : null}
                            onChange={this.handlerInputText}
                            variant="outlined">
                            {this.props.grausAcademicos.map((option) => (
                                <MenuItem key={option.value} value={option.value}>
                                    {option.label}
                                </MenuItem>
                            ))}
                        </TextField>
                    </Grid>
                    <Grid item lg={4}>
                        <TextField
                            name="experiencia"
                            className="TextField"
                            multiline
                            rows={2}
                            required
                            value={this.state.professor.experiencia}
                            onChange={this.handlerInputText}
                            error={this.state.validations.inputExperienciaError}
                            helperText={this.state.validations.inputExperienciaError ? "Campo obrigatório" : null}
                            label="Experiência"
                            variant="outlined"
                            size="small" />
                    </Grid>
                    <Grid
                        container
                        justify="center"
                        style={{ marginTop: 20 }}>
                        <Grid item lg={2}>
                            <Button
                                className="ButtonSave"
                                variant="contained"
                                onClick={this.saveProfessor}>
                                {this.props.selected ? <>Salvar</> : <>Inserir</>}
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
            </div >
        )
    }
}