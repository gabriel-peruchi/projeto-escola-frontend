import '../../App.css';

import {
    Checkbox,
    IconButton,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
} from '@material-ui/core';
import { Delete } from '@material-ui/icons';
import React from 'react';

export default class ListagemProfessores extends React.Component {
    handlerSelect = async (professor) => {
        if (this.isItemSelected(professor._id)) {
            this.refreshSelected()
            return
        }

        await this.props.selectProfessor(professor)
    }

    isItemSelected = (id) => {
        if (!this.props.selected) {
            return false
        }

        return this.props.selected._id === id
    }

    refreshSelected = async () => await this.props.selectProfessor(null)

    deleteProfessor = (id) => {
        if (this.isItemSelected(id)) {
            this.refreshSelected()
        }

        this.props.deleteProfessor(id)
    }

    columns = [
        { label: 'Nome' },
        { label: 'Sobrenome' },
        { label: 'Email' },
        { label: 'Celular' },
        { label: 'Grau acadêmico' },
        { label: 'Experiência' },
    ]

    render() {
        return (
            <Paper>
                <TableContainer>
                    <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>
                                <TableCell className="TableCellHead"></TableCell>
                                {this.columns.map((column) => {
                                    return <TableCell className="TableCellHead" key={column.label}>{column.label}</TableCell>
                                })}
                                <TableCell className="TableCellHead"></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.props.professores.map((e) => {
                                return <TableRow hover key={e._id}>
                                    <TableCell padding="checkbox">
                                        <Checkbox
                                            checked={this.isItemSelected(e._id)}
                                            onChange={() => this.handlerSelect(e)} />
                                    </TableCell>
                                    <TableCell component="th">{e.nome}</TableCell>
                                    <TableCell component="th">{e.sobrenome}</TableCell>
                                    <TableCell component="th">{e.email}</TableCell>
                                    <TableCell component="th">{e.celular}</TableCell>
                                    <TableCell component="th">{(this.props.grausAcademicos.find((ga) => ga.value === e.grauAcademico)).label}</TableCell>
                                    <TableCell component="th">{e.experiencia}</TableCell>
                                    <TableCell align="right">
                                        <IconButton
                                            aria-label="delete"
                                            onClick={() => this.deleteProfessor(e._id)}>
                                            <Delete fontSize="default" />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
        )
    }
}