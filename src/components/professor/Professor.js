import '../../App.css';

import { TextField, Typography } from '@material-ui/core';
import { debounce } from 'lodash';
import React from 'react';

import MessageErrorStatus from '../../core/utils/MessageErrorStatus';
import ProfessorService from '../../services/ProfessorService';
import FormProfessor from './FormProfessor';
import ListagemProfessores from './ListagemProfessores';

const professorService = new ProfessorService()

export default class Professor extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            professores: [],
            selected: null,
            search: ""
        }
    }

    componentDidMount = async () => {
        await this.refreshProfessores()
    }

    selectProfessor = async (professor) => {
        if (!professor) {
            this.setState({ selected: null })
            return
        }

        try {
            const professorDB = await professorService.findById(professor._id)
            this.setState({ selected: professorDB })
        } catch (error) {
            this.props.showSnackBar(MessageErrorStatus(error), 'error')
        }
    }

    handlerSearch = (event) => {
        this.setState({ search: event.target.value })
        debounce(this.refreshProfessores, 300)()
    }

    refreshProfessores = async () => {
        try {
            const professores = await professorService.findAllDocs({ term: this.state.search })
            this.setState({ professores, selected: null })
        } catch (error) {
            this.props.showSnackBar(MessageErrorStatus(error), 'error')
        }
    }

    saveProfessor = async (professor) => {
        try {
            if (this.state.selected) {
                await professorService.update(this.state.selected._id, professor)
            } else {
                await professorService.post(professor)
            }

            this.props.showSnackBar("Salvo com sucesso!", "success")
        } catch (error) {
            this.props.showSnackBar(MessageErrorStatus(error), 'error')
        }

        await this.refreshProfessores()
    }

    deleteProfessor = async (id) => {
        try {
            await professorService.delete(id)
            this.props.showSnackBar("Excluido com sucesso!", "success")
        } catch (error) {
            this.props.showSnackBar(MessageErrorStatus(error), 'error')
        }

        await this.refreshProfessores()
    }

    grausAcademicos = [
        { label: "Selecionar", value: "" },
        { label: "Bacharel", value: "BACHAREL" },
        { label: "Licenciatura", value: "LICENCIATURA" },
        { label: "Tecnólogo", value: "TECNOLOGO" },
        { label: "Pós-graduação", value: "POS_GRADUACAO" },
        { label: "Mestrado", value: "MESTRADO" },
        { label: "Doutorado", value: "DOUTORADO" }
    ]

    render() {
        return (
            <React.Fragment>
                <Typography className="TitlePage" variant="h4" noWrap>
                    Professores
                </Typography>
                <FormProfessor
                    selected={this.state.selected}
                    key={this.state.selected ? this.state.selected._id : null}
                    saveProfessor={this.saveProfessor}
                    grausAcademicos={this.grausAcademicos}>
                </FormProfessor>
                <TextField
                    style={{ marginBottom: 10 }}
                    name="search"
                    value={this.state.search}
                    onChange={this.handlerSearch}
                    label="Buscar"
                    variant="outlined"
                    size="small" />
                <ListagemProfessores
                    professores={this.state.professores}
                    deleteProfessor={this.deleteProfessor}
                    selectProfessor={this.selectProfessor}
                    selected={this.state.selected}
                    grausAcademicos={this.grausAcademicos}>
                </ListagemProfessores>
            </React.Fragment>
        )
    }
}