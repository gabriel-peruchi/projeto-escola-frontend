import '../../App.css';

import { Grid, IconButton, MenuItem, TextField, Typography } from '@material-ui/core';
import { Clear } from '@material-ui/icons';
import { debounce } from 'lodash';
import React from 'react';

import DateUtils from '../../core/utils/DateUtils';
import MessageErrorStatus from '../../core/utils/MessageErrorStatus';
import EstudanteService from '../../services/EstudanteService';
import MatriculaService from '../../services/MatriculaService';
import TurmaService from '../../services/TurmaService';
import FormMatricula from './FormMatricula';
import ListagemMatriculas from './ListagemMatriculas';

const matriculaService = new MatriculaService()
const estudanteService = new EstudanteService()
const turmaService = new TurmaService()

export default class Matricula extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            matriculas: [],
            estudantes: [],
            turmas: [],
            selected: null,
            search: "",
            period: "",
            dateStart: null,
            dateEnd: null
        }
    }

    componentDidMount = async () => {
        await this.refreshMatriculas()

        try {
            const estudantes = await estudanteService.findAllDocs()
            const turmas = await turmaService.findAllDocs()

            this.setState({
                ...this.state,
                estudantes,
                turmas
            })
        } catch (error) {
            this.props.showSnackBar(MessageErrorStatus(error), 'error')
        }
    }

    selectMatricula = async (matricula) => {
        if (!matricula) {
            this.setState({ selected: null })
            return
        }

        try {
            const matriculaDB = await matriculaService.findById(matricula._id)
            this.setState({ selected: matriculaDB })
        } catch (error) {
            this.props.showSnackBar(MessageErrorStatus(error), 'error')
        }
    }

    handlerSearch = (event) => {
        this.setState({ search: event.target.value })
        debounce(this.refreshMatriculas, 300)()
    }

    handlerPeriod = async (event) => {
        let period = {}

        if (event.target.value === "ULTIMOS_SETE_DIAS") {
            period = DateUtils.lastSevenDays()
        } else if (event.target.value === "ULTIMOS_TRINTA_DIAS") {
            period = DateUtils.lastThirtyDays()
        } else if (event.target.value === "ESTA_SEMANA") {
            period = DateUtils.thisWeek()
        } else if (event.target.value === "ESTE_MES") {
            period = DateUtils.thisMonth()
        } else if (event.target.value === "ESTE_ANO") {
            period = DateUtils.thisYear()
        }

        this.setState({
            [event.target.name]: event.target.value,
            dateStart: period.start,
            dateEnd: period.end
        })

        debounce(this.refreshMatriculas, 300)()
    }

    clearPeriod = () => {
        this.setState({
            period: "",
            dateStart: null,
            dateEnd: null
        })
        debounce(this.refreshMatriculas, 300)()
    }

    refreshMatriculas = async () => {
        try {
            const matriculas = await matriculaService.findAllDocs(
                {
                    term: this.state.search,
                    dataInicio: this.state.dateStart,
                    dataFim: this.state.dateEnd,
                }
            )
            this.setState({ ...this.state, matriculas, selected: null, })
        } catch (error) {
            this.props.showSnackBar(MessageErrorStatus(error), 'error')
        }
    }

    saveMatricula = async (matricula) => {
        try {
            if (this.state.selected) {
                await matriculaService.update(this.state.selected._id, matricula)
            } else {
                await matriculaService.post(matricula)
            }

            this.props.showSnackBar("Salvo com sucesso!", "success")
        } catch (error) {
            this.props.showSnackBar(MessageErrorStatus(error), 'error')
        }

        await this.refreshMatriculas()
    }

    deleteMatricula = async (id) => {
        try {
            await matriculaService.delete(id)
            this.props.showSnackBar("Excluido com sucesso!", "success")
        } catch (error) {
            this.props.showSnackBar(MessageErrorStatus(error), 'error')
        }

        await this.refreshMatriculas()
    }

    periods = [
        { label: "Selecionar", value: "" },
        { label: "Utimos 7 dias", value: "ULTIMOS_SETE_DIAS" },
        { label: "Ultimos 30 dias", value: "ULTIMOS_TRINTA_DIAS" },
        { label: "Esta semana", value: "ESTA_SEMANA" },
        { label: "Este mês", value: "ESTE_MES" },
        { label: "Este Ano", value: "ESTE_ANO" },
    ]

    render() {
        return (
            <React.Fragment>
                <Typography className="TitlePage" variant="h4" noWrap>
                    Matrículas
                </Typography>
                <FormMatricula
                    selected={this.state.selected}
                    key={this.state.selected ? this.state.selected._id : null}
                    estudantes={this.state.estudantes}
                    turmas={this.state.turmas}
                    saveMatricula={this.saveMatricula}>
                </FormMatricula>
                <Grid
                    container
                    spacing={2}
                    style={{ marginBottom: 2 }}>
                    <Grid item xs={5} sm={3}>
                        <TextField
                            fullWidth
                            style={{ maxWidth: 220 }}
                            name="search"
                            value={this.state.search}
                            onChange={this.handlerSearch}
                            label="Buscar"
                            variant="outlined"
                            size="small" />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            select
                            fullWidth
                            style={{ maxWidth: 250 }}
                            InputProps={{
                                endAdornment: (
                                    <IconButton
                                        style={{
                                            width: 10,
                                            height: 10,
                                            marginRight: 15,
                                            visibility: this.state.period.trim() ? 'visible' : 'hidden'
                                        }}
                                        onClick={this.clearPeriod}>
                                        <Clear />
                                    </IconButton>
                                )
                            }}
                            id="selectPeriod"
                            name="period"
                            value={this.state.period}
                            onChange={this.handlerPeriod}
                            label="Período da Matrícula"
                            variant="outlined"
                            size="small">
                            {this.periods.map((option) => (
                                <MenuItem key={option.value} value={option.value}>
                                    {option.label}
                                </MenuItem>
                            ))}
                        </TextField>
                    </Grid>
                </Grid>
                <ListagemMatriculas
                    matriculas={this.state.matriculas}
                    deleteMatricula={this.deleteMatricula}
                    selectMatricula={this.selectMatricula}
                    selected={this.state.selected}>
                </ListagemMatriculas>
            </React.Fragment>
        )
    }
}