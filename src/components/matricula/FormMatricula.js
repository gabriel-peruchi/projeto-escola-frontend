import '../../App.css';

import DateFnsUtils from '@date-io/date-fns';
import { Button, Grid, MenuItem, TextField } from '@material-ui/core';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import React from 'react';

export default class FormMatricula extends React.Component {
    constructor(props) {
        super(props)

        const validations = {
            inputIdEstudanteError: false,
            inputIdTurmaError: false
        }

        const matricula = this.props.selected ?
            {
                idEstudante: this.props.selected.estudante._id,
                idTurma: this.props.selected.turma._id,
                dataMatricula: this.props.selected.dataMatricula
            } :
            {
                idEstudante: "",
                idTurma: "",
                dataMatricula: new Date()
            }

        this.state = { matricula, validations }
    }

    handlerInputText = (event) => {
        this.setState({
            matricula: {
                ...this.state.matricula,
                [event.target.name]: event.target.value
            }
        })
    }

    handlerInputDate = (date, value) => {
        this.setState({
            matricula: {
                ...this.state.matricula,
                dataMatricula: date
            }
        })
    }

    saveMatricula = () => {
        if (!this.validationInputs()) return

        const matricula = {
            estudante: this.state.matricula.idEstudante,
            turma: this.state.matricula.idTurma,
            dataMatricula: this.state.matricula.dataMatricula
        }

        this.props.saveMatricula(matricula)
        this.clearForm()
    }

    clearForm = () => {
        this.setState({
            matricula: {
                idEstudante: "",
                idTurma: "",
                dataMatricula: new Date()
            },

            validations: {
                inputIdEstudanteError: false,
                inputIdTurmaError: false
            }
        })
    }

    validationInputs = () => {
        const validationErrors = {}
        let ok = true

        Object.keys(this.state.matricula)
            .forEach((key) => {
                const keyUpperFirst = key[0].toUpperCase() + key.substr(1);
                const keyValidationInputError = `input${keyUpperFirst}Error`

                if (this.state.validations[keyValidationInputError] === undefined) {
                    return
                }

                validationErrors[keyValidationInputError] = !this.state.matricula[key].trim()

                if (!this.state.matricula[key].trim()) {
                    ok = false
                }
            })

        this.setState({ validations: validationErrors })

        return ok
    }

    render() {
        return (
            <div style={{ flexGrow: 1 }}>
                <Grid
                    container
                    justify="center"
                    spacing={2}
                    style={{ marginBottom: 50 }}>
                    <Grid item lg={4}>
                        <TextField
                            name="idEstudante"
                            select
                            className="TextField"
                            required
                            size="small"
                            label="Estudante"
                            value={this.state.matricula.idEstudante}
                            onChange={this.handlerInputText}
                            error={this.state.validations.inputIdEstudanteError}
                            helperText={this.state.validations.inputIdEstudanteError ? "Campo obrigatório" : null}
                            variant="outlined">
                            {this.props.estudantes.map((option) => (
                                <MenuItem key={option._id} value={option._id}>
                                    {option.nome + " " + option.sobrenome}
                                </MenuItem>
                            ))}
                        </TextField>
                    </Grid>
                    <Grid item lg={4}>
                        <TextField
                            name="idTurma"
                            select
                            required
                            className="TextField"
                            size="small"
                            label="Turma"
                            value={this.state.matricula.idTurma}
                            onChange={this.handlerInputText}
                            error={this.state.validations.inputIdTurmaError}
                            helperText={this.state.validations.inputIdTurmaError ? "Campo obrigatório" : null}
                            variant="outlined">
                            {this.props.turmas.map((option) => (
                                <MenuItem key={option._id} value={option._id}>
                                    {option.nome}
                                </MenuItem>
                            ))}
                        </TextField>
                    </Grid>
                    <Grid item lg={4}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                variant="inline"
                                inputVariant="outlined"
                                format="dd/MM/yyyy"
                                className="TextField"
                                size="small"
                                required
                                label="Data da Matricula"
                                name="dataMatricula"
                                value={this.state.matricula.dataMatricula}
                                onChange={this.handlerInputDate}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid
                        container
                        justify="center"
                        style={{ marginTop: 20 }}>
                        <Grid item lg={2}>
                            <Button
                                className="ButtonSave"
                                variant="contained"
                                color="primary"
                                onClick={this.saveMatricula}>
                                {this.props.selected ? <>Salvar</> : <>Inserir</>}
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
            </div >
        )
    }
}