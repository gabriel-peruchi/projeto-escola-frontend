import '../../App.css';

import {
    Checkbox,
    IconButton,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
} from '@material-ui/core';
import { Delete } from '@material-ui/icons';
import React from 'react';

export default class ListagemMatriculas extends React.Component {
    handlerSelect = async (matricula) => {
        if (this.isItemSelected(matricula._id)) {
            this.refreshSelected()
            return
        }

        await this.props.selectMatricula(matricula)
    }

    isItemSelected = (id) => {
        if (!this.props.selected) {
            return false
        }

        return this.props.selected._id === id
    }

    refreshSelected = async () => await this.props.selectMatricula(null)

    deleteMatricula = (id) => {
        if (this.isItemSelected(id)) {
            this.refreshSelected()
        }

        this.props.deleteMatricula(id)
    }

    columns = [
        { label: 'Data da Matricula' },
        { label: 'Estudante' },
        { label: 'Turma' }
    ]

    render() {
        return (
            <TableContainer component={Paper} style={{ maxHeight: 600 }}>
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                            <TableCell className="TableCellHead"></TableCell>
                            {this.columns.map((column) => {
                                return <TableCell className="TableCellHead" key={column.label}>{column.label}</TableCell>
                            })}
                            <TableCell className="TableCellHead"></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.props.matriculas.map((e) => {
                            return <TableRow hover key={e._id}>
                                <TableCell padding="checkbox">
                                    <Checkbox
                                        checked={this.isItemSelected(e._id)}
                                        onChange={() => this.handlerSelect(e)} />
                                </TableCell>
                                <TableCell component="th">{new Date(e.dataMatricula).toLocaleDateString()}</TableCell>
                                <TableCell component="th">{e.estudante.nome + " " + e.estudante.sobrenome}</TableCell>
                                <TableCell component="th">{e.turma.nome}</TableCell>
                                <TableCell align="right">
                                    <IconButton
                                        aria-label="delete"
                                        onClick={() => this.deleteMatricula(e._id)}>
                                        <Delete fontSize="default" />
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
        )
    }
}