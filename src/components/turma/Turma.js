import '../../App.css';

import { TextField, Typography } from '@material-ui/core';
import { debounce } from 'lodash';
import React from 'react';

import MessageErrorStatus from '../../core/utils/MessageErrorStatus';
import DisciplinaService from '../../services/DisciplinaService';
import ProfessorService from '../../services/ProfessorService';
import TurmaService from '../../services/TurmaService';
import FormTurma from './FormTurma';
import ListagemTurmas from './ListagemTurmas';

const turmaService = new TurmaService()
const disciplinaService = new DisciplinaService()
const professorService = new ProfessorService()

export default class Turma extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            turmas: [],
            professores: [],
            disciplinas: [],
            selected: null,
            search: ""
        }
    }

    componentDidMount = async () => {
        await this.refreshTurmas()

        try {
            const professores = await professorService.findAllDocs()
            const disciplinas = await disciplinaService.findAllDocs()
            this.setState({
                ...this.state,
                professores,
                disciplinas
            })
        } catch (error) {
            this.props.showSnackBar(MessageErrorStatus(error), 'error')
        }
    }

    selectTurma = async (turma) => {
        if (!turma) {
            this.setState({ selected: null })
            return
        }

        try {
            const turmaDB = await turmaService.findById(turma._id)
            this.setState({ selected: turmaDB })
        } catch (error) {
            this.props.showSnackBar(MessageErrorStatus(error), 'error')
        }
    }

    handlerSearch = (event) => {
        this.setState({ search: event.target.value })
        debounce(this.refreshTurmas, 300)()
    }

    refreshTurmas = async () => {
        try {
            const turmas = await turmaService.findAllDocs({ term: this.state.search })
            this.setState({ ...this.state, turmas, selected: null, })
        } catch (error) {
            this.props.showSnackBar(MessageErrorStatus(error), 'error')
        }
    }

    saveTurma = async (turma) => {
        try {
            if (this.state.selected) {
                await turmaService.update(this.state.selected._id, turma)
            } else {
                await turmaService.post(turma)
            }

            this.props.showSnackBar("Salvo com sucesso!", "success")
        } catch (error) {
            this.props.showSnackBar(MessageErrorStatus(error), 'error')
        }

        await this.refreshTurmas()
    }

    deleteTurma = async (id) => {
        try {
            await turmaService.delete(id)
            this.props.showSnackBar("Excluido com sucesso!", "success")
        } catch (error) {
            this.props.showSnackBar(MessageErrorStatus(error), 'error')
        }

        await this.refreshTurmas()
    }

    render() {
        return (
            <React.Fragment>
                <Typography className="TitlePage" variant="h4" noWrap>
                    Turmas
                </Typography>
                <FormTurma
                    selected={this.state.selected}
                    key={this.state.selected ? this.state.selected._id : null}
                    disciplinas={this.state.disciplinas}
                    professores={this.state.professores}
                    saveTurma={this.saveTurma}>
                </FormTurma>
                <TextField
                    style={{ marginBottom: 10 }}
                    name="search"
                    value={this.state.search}
                    onChange={this.handlerSearch}
                    label="Buscar"
                    variant="outlined"
                    size="small" />
                <ListagemTurmas
                    turmas={this.state.turmas}
                    disciplinas={this.state.disciplinas}
                    professores={this.state.professores}
                    deleteTurma={this.deleteTurma}
                    selectTurma={this.selectTurma}
                    selected={this.state.selected}>
                </ListagemTurmas>
            </React.Fragment>
        )
    }
}