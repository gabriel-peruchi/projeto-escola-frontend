import '../../App.css';

import { Button, Grid, MenuItem, TextField } from '@material-ui/core';
import React from 'react';

export default class FormTurma extends React.Component {
    constructor(props) {
        super(props)

        const validations = {
            inputNomeError: false,
            inputIdProfessorError: false,
            inputIdDisciplinaError: false,
        }

        const turma = this.props.selected ?
            {
                nome: this.props.selected.nome,
                idProfessor: this.props.selected.professor._id,
                idDisciplina: this.props.selected.disciplina._id
            } :
            {
                nome: "",
                idProfessor: "",
                idDisciplina: ""
            }

        this.state = { turma, validations }
    }

    handlerInputText = (event) => {
        this.setState({
            turma: {
                ...this.state.turma,
                [event.target.name]: event.target.value
            }
        })
    }

    saveTurma = () => {
        if (!this.validationInputs()) return

        const turma = {
            nome: this.state.turma.nome,
            professor: this.state.turma.idProfessor,
            disciplina: this.state.turma.idDisciplina
        }

        this.props.saveTurma(turma)
        this.clearForm()
    }

    clearForm = () => {
        this.setState({
            turma: {
                nome: "",
                idProfessor: "",
                idDisciplina: ""
            },

            validations: {
                inputNomeError: false,
                inputIdProfessorError: false,
                inputIdDisciplinaError: false,
            }
        })
    }

    validationInputs = () => {
        const validationErrors = {}
        let ok = true

        Object.keys(this.state.turma)
            .forEach((key) => {
                const keyUpperFirst = key[0].toUpperCase() + key.substr(1);
                const keyValidationInputError = `input${keyUpperFirst}Error`

                if (this.state.validations[keyValidationInputError] === undefined) {
                    return
                }

                validationErrors[keyValidationInputError] = !this.state.turma[key].trim()

                if (!this.state.turma[key].trim()) {
                    ok = false
                }
            })

        this.setState({ validations: validationErrors })

        return ok
    }

    render() {
        return (
            <div style={{ flexGrow: 1 }}>
                <Grid
                    container
                    justify="center"
                    spacing={2}
                    style={{ marginBottom: 50 }}>
                    <Grid item lg={4}>
                        <TextField
                            name="nome"
                            className="TextField"
                            required
                            value={this.state.turma.nome}
                            onChange={this.handlerInputText}
                            error={this.state.validations.inputNomeError}
                            helperText={this.state.validations.inputNomeError ? "Campo obrigatório" : null}
                            label="Nome"
                            variant="outlined"
                            size="small" />
                    </Grid>
                    <Grid item lg={4}>
                        <TextField
                            name="idProfessor"
                            required
                            className="TextField"
                            size="small"
                            select
                            label="Professor"
                            value={this.state.turma.idProfessor}
                            onChange={this.handlerInputText}
                            error={this.state.validations.inputIdProfessorError}
                            helperText={this.state.validations.inputIdProfessorError ? "Campo obrigatório" : null}
                            variant="outlined">
                            {this.props.professores.map((option) => (
                                <MenuItem key={option._id} value={option._id}>
                                    {option.nome + " " + option.sobrenome}
                                </MenuItem>
                            ))}
                        </TextField>
                    </Grid>
                    <Grid item lg={4}>
                        <TextField
                            name="idDisciplina"
                            size="small"
                            required
                            className="TextField"
                            select
                            label="Disciplina"
                            value={this.state.turma.idDisciplina}
                            onChange={this.handlerInputText}
                            error={this.state.validations.inputIdDisciplinaError}
                            helperText={this.state.validations.inputIdDisciplinaError ? "Campo obrigatório" : null}
                            variant="outlined">
                            {this.props.disciplinas.map((option) => (
                                <MenuItem key={option._id} value={option._id}>
                                    {option.nome}
                                </MenuItem>
                            ))}
                        </TextField>
                    </Grid>
                    <Grid
                        container
                        justify="center"
                        style={{ marginTop: 20 }}>
                        <Grid item lg={2}>
                            <Button
                                className="ButtonSave"
                                variant="contained"
                                color="primary"
                                onClick={this.saveTurma}>
                                {this.props.selected ? <>Salvar</> : <>Inserir</>}
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        )
    }
}