import '../../App.css';

import {
    Checkbox,
    IconButton,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
} from '@material-ui/core';
import { Delete } from '@material-ui/icons';
import React from 'react';

export default class ListagemTurmas extends React.Component {
    handlerSelect = async (turma) => {
        if (this.isItemSelected(turma._id)) {
            this.refreshSelected()
            return
        }

        await this.props.selectTurma(turma)
    }

    isItemSelected = (id) => {
        if (!this.props.selected) {
            return false
        }

        return this.props.selected._id === id
    }

    refreshSelected = async () => await this.props.selectTurma(null)

    deleteTurma = (id) => {
        if (this.isItemSelected(id)) {
            this.refreshSelected()
        }

        this.props.deleteTurma(id)
    }

    columns = [
        { label: 'Nome' },
        { label: 'Professor' },
        { label: 'Disciplina' }
    ]

    render() {
        return (
            <Paper>
                <TableContainer>
                    <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>
                                <TableCell className="TableCellHead"></TableCell>
                                {this.columns.map((column) => {
                                    return <TableCell className="TableCellHead" key={column.label}>{column.label}</TableCell>
                                })}
                                <TableCell className="TableCellHead"></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.props.turmas.map((e) => {
                                return <TableRow hover key={e._id}>
                                    <TableCell padding="checkbox">
                                        <Checkbox
                                            checked={this.isItemSelected(e._id)}
                                            onChange={() => this.handlerSelect(e)} />
                                    </TableCell>
                                    <TableCell component="th">{e.nome}</TableCell>
                                    <TableCell component="th">{e.professor.nome + " " + e.professor.sobrenome}</TableCell>
                                    <TableCell component="th">{e.disciplina.nome}</TableCell>
                                    <TableCell align="right">
                                        <IconButton
                                            aria-label="delete"
                                            onClick={() => this.deleteTurma(e._id)}>
                                            <Delete fontSize="default" />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
        )
    }
}