import { Card, CardContent, Divider, List, ListItem, ListItemText, Typography } from '@material-ui/core';
import React from 'react';

export default class CardTurma extends React.Component {
    render() {
        return (
            <Card style={{ width: 260 }}>
                <CardContent>
                    <Typography variant="h5" component="h2">
                        {this.props.turma}
                    </Typography>
                    <Typography color="textSecondary">
                        {`Disciplina: ${this.props.disciplina.nome}`}
                    </Typography>
                    <Typography color="textSecondary">
                        {`Prof.(a): ${this.props.professor.nome} ${this.props.professor.sobrenome}`}
                    </Typography>
                    <Divider style={{ marginTop: 5 }} />
                    <Typography style={{ marginTop: 12 }} variant="body1" component="p">
                        {`${this.props.qtdMatriculas} matrículas`}
                    </Typography>
                    <List dense className="ListEstudents">
                        {this.props.estudantes.map((e) => (
                            <ListItem key={e.nome}>
                                <ListItemText primary={`${e.nome} ${e.sobrenome}`} />
                            </ListItem>
                        ))}
                    </List>
                </CardContent>
            </Card>
        )
    }
}