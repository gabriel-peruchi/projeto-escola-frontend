import '../../App.css';

import { Grid, Typography } from '@material-ui/core';
import React from 'react';

import MessageErrorStatus from '../../core/utils/MessageErrorStatus';
import DashboardService from '../../services/DashboardService';
import CardTurma from './CardTurma';

const dashboardService = new DashboardService()

export default class Dashboard extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            turmas: []
        }
    }

    componentDidMount = async () => {
        try {
            const turmas = await dashboardService.findAllDocs()
            this.setState({ turmas })
        } catch (error) {
            this.props.showSnackBar(MessageErrorStatus(error), 'error')
        }
    }

    render() {
        return (
            <React.Fragment>
                <Typography className="TitlePage" variant="h4" noWrap>
                    Dashboard
                </Typography>
                <Grid container spacing={2}>
                    {this.state.turmas.map(turma => (
                        <Grid key={turma._id} item lg={3}>
                            <CardTurma
                                turma={turma.nome}
                                professor={turma.professor}
                                disciplina={turma.disciplina}
                                qtdMatriculas={turma.qtdMatriculas}
                                estudantes={turma.estudantes}
                            />
                        </Grid>
                    ))}
                </Grid>
            </React.Fragment>
        )
    }
}