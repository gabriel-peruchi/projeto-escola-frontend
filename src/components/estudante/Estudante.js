import '../../App.css';

import { TextField, Typography } from '@material-ui/core';
import { debounce } from 'lodash';
import React from 'react';

import MessageErrorStatus from '../../core/utils/MessageErrorStatus';
import EstudanteService from '../../services/EstudanteService';
import FormEstudante from './FormEstudante';
import ListagemEstudantes from './ListagemEstudantes';

const estudanteService = new EstudanteService()

export default class Estudante extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            estudantes: [],
            selected: null,
            search: ""
        }
    }

    componentDidMount = async () => {
        await this.refreshEstudantes()
    }

    selectEstudante = async (estudante) => {
        if (!estudante) {
            this.setState({ selected: null })
            return
        }

        try {
            const estudanteDB = await estudanteService.findById(estudante._id)
            this.setState({ selected: estudanteDB })
        } catch (error) {
            this.props.showSnackBar(MessageErrorStatus(error), 'error')
        }
    }

    handlerSearch = (event) => {
        this.setState({ search: event.target.value })
        debounce(this.refreshEstudantes, 300)()
    }

    refreshEstudantes = async () => {
        try {
            const estudantes = await estudanteService.findAllDocs({ term: this.state.search })
            this.setState({ estudantes, selected: null })
        } catch (error) {
            this.props.showSnackBar(MessageErrorStatus(error), 'error')
        }
    }

    saveEstudante = async (estudante) => {
        try {
            if (this.state.selected) {
                await estudanteService.update(this.state.selected._id, estudante)
            } else {
                await estudanteService.post(estudante)
            }
            this.props.showSnackBar("Salvo com sucesso!", "success")
        } catch (error) {
            this.props.showSnackBar(MessageErrorStatus(error), 'error')
        }

        await this.refreshEstudantes()
    }

    deleteEstudante = async (id) => {
        try {
            await estudanteService.delete(id)
            this.props.showSnackBar("Excluido com sucesso!", "success")
        } catch (error) {
            this.props.showSnackBar(MessageErrorStatus(error), 'error')
        }

        await this.refreshEstudantes()
    }

    render() {
        return (
            <React.Fragment>
                <Typography className="TitlePage" variant="h4" noWrap>
                    Estudantes
                </Typography>
                <FormEstudante
                    selected={this.state.selected}
                    key={this.state.selected ? this.state.selected._id : null}
                    saveEstudante={this.saveEstudante}>
                </FormEstudante>
                <TextField
                    style={{ marginBottom: 10 }}
                    name="search"
                    value={this.state.search}
                    onChange={this.handlerSearch}
                    label="Buscar"
                    variant="outlined"
                    size="small" />
                <ListagemEstudantes
                    estudantes={this.state.estudantes}
                    deleteEstudante={this.deleteEstudante}
                    selectEstudante={this.selectEstudante}
                    selected={this.state.selected}>
                </ListagemEstudantes>
            </React.Fragment>
        )
    }
}