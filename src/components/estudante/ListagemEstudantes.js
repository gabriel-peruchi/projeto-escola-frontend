import '../../App.css'

import {
    Checkbox,
    IconButton,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
} from '@material-ui/core';
import { Delete } from '@material-ui/icons';
import React from 'react';

export default class ListagemEstudantes extends React.Component {
    handlerSelect = async (estudante) => {
        if (this.isItemSelected(estudante._id)) {
            this.refreshSelected()
            return
        }

        await this.props.selectEstudante(estudante)
    }

    isItemSelected = (id) => {
        if (!this.props.selected) {
            return false
        }

        return this.props.selected._id === id
    }

    refreshSelected = async () => await this.props.selectEstudante(null)

    deleteEstudante = (id) => {
        if (this.isItemSelected(id)) {
            this.refreshSelected()
        }

        this.props.deleteEstudante(id)
    }

    columns = [
        { label: 'Nome' },
        { label: 'Sobrenome' },
        { label: 'CPF' },
        { label: 'Email' },
        { label: 'Celular' },
    ]

    render() {
        return (
            <Paper>
                <TableContainer>
                    <Table stickyHeader aria-label="sticky table" size="medium">
                        <TableHead>
                            <TableRow>
                                <TableCell className="TableCellHead"></TableCell>
                                {this.columns.map((column) => {
                                    return <TableCell className="TableCellHead" key={column.label}>{column.label}</TableCell>
                                })}
                                <TableCell className="TableCellHead"></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.props.estudantes.map((e) => {
                                return <TableRow hover key={e._id}>
                                    <TableCell padding="checkbox">
                                        <Checkbox
                                            checked={this.isItemSelected(e._id)}
                                            onChange={() => this.handlerSelect(e)} />
                                    </TableCell>
                                    <TableCell component="th">{e.nome}</TableCell>
                                    <TableCell component="th">{e.sobrenome}</TableCell>
                                    <TableCell component="th">{e.cpf}</TableCell>
                                    <TableCell component="th">{e.email}</TableCell>
                                    <TableCell component="th">{e.celular}</TableCell>
                                    <TableCell align="right">
                                        <IconButton
                                            aria-label="delete"
                                            onClick={() => this.deleteEstudante(e._id)}>
                                            <Delete fontSize="default" />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
        )
    }
}