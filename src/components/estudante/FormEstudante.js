import '../../App.css';

import DateFnsUtils from '@date-io/date-fns';
import { Button, Grid, TextField } from '@material-ui/core';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import React from 'react';

import TextCellPhoneFormat from '../generics/TextCellPhoneFormat';
import TextCpfFormat from '../generics/TextCpfFormat';
import TextPhoneFormat from '../generics/TextPhoneFormat';

export default class FormEstudante extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            estudante: this.props.selected ? this.props.selected : {
                nome: "",
                sobrenome: "",
                cpf: "",
                dataNascimento: new Date(),
                email: "",
                celular: "",
                telefoneFixo: ""
            },
            validations: {
                inputNomeError: false,
                inputSobrenomeError: false,
                inputCpfError: false,
                inputEmailError: false,
                inputCelularError: false,
            }
        }
    }

    handlerInputText = (event) => {
        this.setState({
            estudante: {
                ...this.state.estudante,
                [event.target.name]: event.target.value
            }
        })
    }

    handlerInputDate = (date, value) => {
        this.setState({
            estudante: {
                ...this.state.estudante,
                dataNascimento: date
            }
        })
    }

    saveEstudante = () => {
        if (!this.validationInputs()) return

        this.props.saveEstudante(this.state.estudante)
        this.clearForm()
    }

    clearForm = () => {
        this.setState({
            estudante: {
                nome: "",
                sobrenome: "",
                cpf: "",
                dataNascimento: new Date(),
                email: "",
                celular: "",
                telefoneFixo: ""
            },
            validations: {
                inputNomeError: false,
                inputSobrenomeError: false,
                inputCpfError: false,
                inputEmailError: false,
                inputCelularError: false,
            }
        })
    }

    validationInputs = () => {
        const validationErrors = {}
        let ok = true

        Object.keys(this.state.estudante)
            .forEach((key) => {
                const keyUpperFirst = key[0].toUpperCase() + key.substr(1);
                const keyValidationInputError = `input${keyUpperFirst}Error`

                if (this.state.validations[keyValidationInputError] === undefined) {
                    return
                }

                validationErrors[keyValidationInputError] = !this.state.estudante[key].trim()

                if (!this.state.estudante[key].trim()) {
                    ok = false
                }
            })

        this.setState({ validations: validationErrors })

        return ok
    }

    render() {
        return (
            <div style={{ flexGrow: 1 }}>
                <Grid
                    container
                    spacing={2}
                    style={{ marginBottom: 50 }}>
                    <Grid item lg={4}>
                        <TextField
                            name="nome"
                            className="TextField"
                            required
                            value={this.state.estudante.nome}
                            onChange={this.handlerInputText}
                            error={this.state.validations.inputNomeError}
                            helperText={this.state.validations.inputNomeError ? "Campo obrigatório" : null}
                            label="Nome"
                            variant="outlined"
                            size="small" />
                    </Grid>
                    <Grid item lg={4}>
                        <TextField
                            name="sobrenome"
                            className="TextField"
                            required
                            value={this.state.estudante.sobrenome}
                            onChange={this.handlerInputText}
                            error={this.state.validations.inputSobrenomeError}
                            helperText={this.state.validations.inputSobrenomeError ? "Campo obrigatório" : null}
                            label="Sobrenome"
                            variant="outlined"
                            size="small" />
                    </Grid>
                    <Grid item lg={4}>
                        <TextField
                            name="cpf"
                            className="TextField"
                            required
                            value={this.state.estudante.cpf}
                            onChange={this.handlerInputText}
                            error={this.state.validations.inputCpfError}
                            helperText={this.state.validations.inputCpfError ? "Campo obrigatório" : null}
                            InputProps={{ inputComponent: TextCpfFormat }}
                            label="CPF"
                            variant="outlined"
                            size="small" />
                    </Grid>
                    <Grid item lg={4}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                required
                                variant="inline"
                                inputVariant="outlined"
                                size="small"
                                format="dd/MM/yyyy"
                                label="Data de Nacimento"
                                name="dataNascimento"
                                className="TextField"
                                value={this.state.estudante.dataNascimento}
                                onChange={this.handlerInputDate}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item lg={4}>
                        <TextField
                            name="celular"
                            className="TextField"
                            required
                            value={this.state.estudante.celular}
                            onChange={this.handlerInputText}
                            error={this.state.validations.inputCelularError}
                            helperText={this.state.validations.inputCelularError ? "Campo obrigatório" : null}
                            InputProps={{ inputComponent: TextCellPhoneFormat }}
                            label="Celular"
                            variant="outlined"
                            size="small" />
                    </Grid>
                    <Grid item lg={4}>
                        <TextField
                            name="telefoneFixo"
                            className="TextField"
                            value={this.state.estudante.telefoneFixo}
                            onChange={this.handlerInputText}
                            error={this.state.validations.inputTelefoneFixoError}
                            helperText={this.state.validations.inputTelefoneFixoError ? "Campo obrigatório" : null}
                            InputProps={{ inputComponent: TextPhoneFormat }}
                            label="Telefone Fixo"
                            variant="outlined"
                            size="small" />
                    </Grid>
                    <Grid item lg={4}>
                        <TextField
                            className="TextField"
                            name="email"
                            required
                            value={this.state.estudante.email}
                            onChange={this.handlerInputText}
                            error={this.state.validations.inputEmailError}
                            helperText={this.state.validations.inputEmailError ? "Campo obrigatório" : null}
                            label="Email"
                            variant="outlined"
                            size="small" />
                    </Grid>
                    <Grid
                        container
                        justify="center"
                        style={{ marginTop: 20 }}>
                        <Grid item lg={2}>
                            <Button
                                variant="contained"
                                className="ButtonSave"
                                onClick={this.saveEstudante}>
                                {this.props.selected ? <>Salvar</> : <>Inserir</>}
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
            </div >
        )
    }
}