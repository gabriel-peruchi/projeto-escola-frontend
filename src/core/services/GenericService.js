import Axios from 'axios'

const URL_API = 'http://localhost:3000'
const BEARER_TOKEN = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c3VhcmlvSWQiOiI1ZTcyNjFlYzFmMTQ1YjMwYTAwODdlNDAiLCJ0ZW5hbnRJZCI6IjVlNzI2MTRmMWYxNDViMzBhMDA4N2UzZSIsImlhdCI6MTYwMzg5NjczMn0.ml3Z12rjINe6F7Lvyl8iAsnq7nGtcUCvHxGSP9HoIy8'


Axios.interceptors.response.use(
    response => {
        return Promise.resolve(response)
    },
    error => {
        return Promise.reject(error)
    }
)

export class GenericService {
    constructor(model) {
        this.model = model
    }

    async findAllDocs(options = {}) {
        const result = await Axios.get(
            `${URL_API}/${this.model}`,
            { headers: { Authorization: BEARER_TOKEN }, params: { limit: 200, ...options } }
        )
        return result.data
    }

    async findById(id) {
        const result = await Axios.get(
            `${URL_API}/${this.model}/${id}`,
            { headers: { Authorization: BEARER_TOKEN } }
        )

        return result.data
    }

    async post(doc) {
        const result = await Axios.post(
            `${URL_API}/${this.model}`,
            doc,
            { headers: { Authorization: BEARER_TOKEN } }
        )

        return result.data
    }

    async update(id, doc) {
        const result = await Axios.put(
            `${URL_API}/${this.model}/${id}`,
            doc,
            { headers: { Authorization: BEARER_TOKEN } }
        )

        return result.data
    }

    async delete(id) {
        const result = await Axios.delete(
            `${URL_API}/${this.model}/${id}`,
            { headers: { Authorization: BEARER_TOKEN } }
        )

        return result.data
    }
}
