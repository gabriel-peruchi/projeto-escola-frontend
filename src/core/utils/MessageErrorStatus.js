export default function getMessageError(error) {
    if (!error.response) {
        return 'Nosso serviço está temporariamente indisponível.'
    }

    let message = ''

    switch (error.response.status) {
        case 500:
            message = 'Um erro inesperado ocorreu. Tente novamente.'
            break
        case 400:
            message = error.response.data?.message || 'Um erro inesperado ocorreu. Tente novamente.'
            break
        case 502:
            message = 'Nosso serviço está temporariamente indisponível.'
            break
        case 401:
            message = 'Você não tem permissão para realizar a ação solicitada'
            break
        default:
            message = 'Um erro insperado ocorreu. Tente novamente.'
            break
    }

    return message
}