import dayjs from "dayjs";

export default class DateUtils {

    static lastSevenDays() {
        return {
            start: dayjs().subtract(7, 'day').format(),
            end: dayjs().format()
        }
    }

    static lastThirtyDays() {
        return {
            start: dayjs().subtract(30, 'day').format(),
            end: dayjs().format()
        }
    }

    static thisWeek() {
        return {
            start: dayjs().startOf('week').format(),
            end: dayjs().endOf('week').format()
        }
    }

    static thisMonth() {
        return {
            start: dayjs().startOf('month').format(),
            end: dayjs().endOf('month').format()
        }
    }

    static thisYear() {
        return {
            start: dayjs().startOf('year').format(),
            end: dayjs().endOf('year').format()
        }
    }
}